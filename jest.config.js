const jestConfig = {
  testMatch: [
    '<rootDir>/test/unit/**.test.js'
  ],
  verbose: true,
  setupFiles: [
    '<rootDir>/test/unit/setup-jest.js'
  ]
}

module.exports = jestConfig
