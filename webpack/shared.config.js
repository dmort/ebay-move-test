const path = require('path')
const webpack = require('webpack')

// Shared webpack settings
// -----------------------
const shared = {
  webpack: {
    entry: [
      // fetch polyfill for older browsers
      'whatwg-fetch',

      // the entry point of our app
      './src/client/index.js'
    ],

    output: {
      path: path.resolve(__dirname, '../static/'),
      filename: 'client.js',
    },

    module: {
      rules: [
        {
          test: /\.js$/,
          include: [
            // Only include the client directory in transforms.
            path.resolve(__dirname, '../src/client/'),
          ],
          use: [
            // Cache the result of the transform on disk.
            'cache-loader',
            // Use Babel to transform js.
            'babel-loader'
          ]
        }
      ]
    },

    plugins: [
    ],

    performance: {
      hints: false
    }
  }
}

module.exports = shared
