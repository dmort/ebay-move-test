const webpack = require('webpack')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

const sharedConfig = require('./shared.config.js').webpack

const productionConfig = Object.assign(sharedConfig, {
  mode: 'production',
  plugins: sharedConfig.plugins.concat([
    // Minimize the JavaScript.
    new UglifyJsPlugin()
  ])
})

module.exports = productionConfig
