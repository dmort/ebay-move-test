const path = require('path')
const webpack = require('webpack')

const shared = require('./shared.config.js')

const sharedConfig = shared.webpack

const devServerPort = 5000

const developmentConfig = Object.assign(sharedConfig, {
  mode: 'development',
  entry: [
    // activate HMR for React
    'react-hot-loader/patch',

    // bundle the client for webpack-dev-server
    // and connect to the provided endpoint
    `webpack-dev-server/client?http://localhost:${devServerPort}`,

    // bundle the client for hot reloading
    // only- means to only hot reload for successful updates
    'webpack/hot/only-dev-server',
  ].concat(sharedConfig.entry),

  output: Object.assign(sharedConfig.output, {
    publicPath: `http://localhost:${devServerPort}/`,
  }),

  devtool: 'cheap-module-eval-source-map',

  plugins: [
    // enable HMR globally
    new webpack.HotModuleReplacementPlugin(),

    // print more readable module names in the browser console on HMR updates
    new webpack.NamedModulesPlugin()
  ].concat(sharedConfig.plugins),

  devServer: {
    // Serve all assets from this path.
    contentBase: path.resolve(__dirname, '../assets/'),

    // Serve the dev assets at localhost:9000
    port: devServerPort,

    // Enable HMR on the dev server
    hot: true,

    // Allow hot updates to be served from the dev server port.
    headers: {
      "Access-Control-Allow-Origin": "*"
    }
  }
})

module.exports = developmentConfig
