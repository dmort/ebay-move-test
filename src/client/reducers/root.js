// The root reducer responds to actions with changes to the root level
// of the global state object.  It also initializes branches and performs
// cross-sectional changes, i.e. changes across multiple branches of the state.
// ----------------------------------------------------------------------------

import * as actions from '../actions/index'

function root (state, action) {
  switch (action.type) {
    default:
      return state
  }
}

export default root
