// The index for the redux reducer.  After an action is dispatched from
// somewhere in the client, the global state object passes first through here
// and is reduced by individual functions, which are imported here in this file,
// and manage state transitions for branches of the global state object.
// ----------------------------------------------------------------------------

import root from './root'
import * as actions from '../actions/index'

const initialState = {}

function reducer (state, action) {
  // If there's no previous state, create an initial state.
  if (state === undefined)
    return initialState

  // Create a new state from the root reducer, including root-level state
  // attributes and cross-sectional transitions.
  const rootState = root(state, action)

  // Return the cross-sectional, global state reducer's state.
  return {
    ...rootState
  }
}

export default reducer
