// Container component for a listing of images
// -------------------------------------------

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import exact from 'prop-types-exact'
import Grid from '@material-ui/core/Grid'
import { NavLink } from 'react-router-dom'

import routeProps from './helpers/routeProps'
import Image from './Image'

const ImageList = (props) => {
  return (
    <Grid container>
      {
          props.images.map((image, index) => {
          return (
            <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
              <NavLink to={image.viewImageUrl}>
                <img
                  style={{ display: "block", margin: "auto", padding: 50 }}
                  src={image.urlSmall}
                />
              </NavLink>
            </Grid>
          )

        })
      }
    </Grid>
  )
}

ImageList.propTypes = exact({
  images: PropTypes.array.isRequired,
  ...routeProps
})

export default ImageList
