import React from 'react'
import { storiesOf } from '@storybook/react'
import { frame, renderForm } from "../../../test/helpers/storybook-helpers"
import { withInfo } from "@storybook/addon-info"
import { MemoryRouter } from 'react-router'

import Image from './Image'
import ImageList from './ImageList'
import parsedCar from '../../../test/unit/fixtures/responses/parsed/car'

const imageProps = {
  uri: 'i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$',
  set: 'fe4cfedffdffffffff',
  urlSmall: 'https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$_2.jpg',
  urlLarge: 'https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$_27.jpg',
  viewImageUrl: '/97045/images/0'
}

const images = parsedCar.images

storiesOf('Image', module)
  .addDecorator(frame('Image', 500))
  .add(
    'Full-screen image',
    withInfo(
      `
      The Image component renders a single image to be rendered on a View
      Image Page.
    `
    )(() => (
      <Image image={imageProps} />
    ))
  )

storiesOf('ImageList', module)
  .addDecorator(frame('Image', 800))
  .addDecorator(story => (
    <MemoryRouter initialEntries={['/']}>{story()}</MemoryRouter>
  ))
  .add(
    'A List of car images',
    withInfo(
      `
      The ImageList component renders all of a car's images as thumbnails`
    )(() => (
      <ImageList images={images} />
    ))
  )
