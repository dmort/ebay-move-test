// A default set of props for a component which is rendered with
// react-router-dom
// -------------------------------------------------------------

import PropTypes from 'prop-types'

export default {
  match: PropTypes.object,
  location: PropTypes.object,
  history: PropTypes.object,
  staticContext: PropTypes.object
}
