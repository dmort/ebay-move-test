// The app header
// --------------

import React from 'react'
import PropTypes from 'prop-types'
import exact from 'prop-types-exact'
import { withStyles } from '@material-ui/core/styles'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import CloseIcon from '@material-ui/icons/Close'
import { NavLink } from 'react-router-dom'

import routeProps from './helpers/routeProps'

const styles = theme => ({
  root: {
    width: '100%',
  },
  toolbar: {
    backgroundColor: theme.palette.primary[900],
    color: theme.palette.primary[300]
  },
  button: {
    backgroundColor: theme.palette.primary[900],
    color: 'white',
    width: 35,
    height: 35,
    '&:hover': {
      color: theme.palette.primary[900]
    }
  },
  flex: {
    flex: 1
  }
})

const Header = (props) => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <AppBar position="static" elevation={1}>
        <Toolbar className={classes.toolbar}>
          <Typography
            variant="title"
            color="inherit"
            className={classes.flex}
          >
            {props.title}
          </Typography>

          {props.match.params.id &&
            <NavLink to="/">
              <Button
                variant="fab"
                aria-label="add"
                disabled={props.addNoteDisabled}
                className={classes.button}
                elevation={0}
              >
                <CloseIcon />
              </Button>
            </NavLink>
          }
        </Toolbar>
      </AppBar>
    </div>
  )
}

Header.propTypes = exact({
  classes: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  ...routeProps
})

export default withStyles(styles)(Header)
