import React from 'react'
import PropTypes from 'prop-types'
import exact from 'prop-types-exact'
import Grid from '@material-ui/core/Grid'
import { withStyles } from '@material-ui/core/styles'

import routeProps from './helpers/routeProps'

const styles = theme => ({
  container: {
    height: `calc(100vh - ${theme.mixins.toolbar.minHeight}px - ${theme.spacing.unit * 2}px)`
  }
})

const Image = (props) => (
  <Grid
    container
    justify="center"
    alignItems="center"
    spacing={40}
    className={props.classes.container}
  >
    <Grid item>
      <img
        style={{
          maxWidth: '100%',
        }}
        src={props.image.urlLarge}
      />
    </Grid>
  </Grid>
)

Image.propTypes = exact({
  classes: PropTypes.object.isRequired,
  image: PropTypes.object.isRequired,
  ...routeProps
})

export default withStyles(styles)(Image)
