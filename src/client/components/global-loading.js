// A global loading indicator
// --------------------------

import React from 'react'
import CircularProgress from '@material-ui/core/CircularProgress'

const GlobalLoading = () => {
  return (
    <CircularProgress style={{
      position: 'fixed',
      top: '50%',
      left: '50%'
    }} />
  )
}

export default GlobalLoading
