// The root React component for the client
// The Redux store is created here and passed into the react-redux Provider.
// -------------------------------------------------------------------------

import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import thunk from 'redux-thunk'

import reducer from '../reducers/index'
import App from './App'
import createStore from '../../store/create'
import muiTheme from '../styles/mui-theme'

const theme = createMuiTheme(muiTheme)

// Enable Redux dev tools if they exist.
const composeEnhanced = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

// Create the Redux store with the preloaded state.
const store = createStore(window.__PRELOADED_STATE__, composeEnhanced)

const root = (props) => {
  return (
    <Provider store={store}>
      <Router>
        <MuiThemeProvider theme={theme}>
          <Route path="/" component={App} />
        </MuiThemeProvider>
      </Router>
    </Provider>
  )
}

export default root
