// The main container component for the client
// -------------------------------------------

import React, { Component } from 'react'
import PropTypes from 'prop-types'
import exact from 'prop-types-exact'
import { connect } from 'react-redux'
import { Route } from 'react-router-dom'
import Grid from '@material-ui/core/Grid'

import routeProps from './helpers/routeProps'
import Header from './header'
import GlobalLoading from './global-loading'
import ImageList from './ImageList'
import Image from './Image'

class App extends Component {
  render () {
    let content = null

    // If the necessary data for the app is loaded, render the top-level routes.
    if (this.props.car) {
      content = (
        <div>
          <Route exact path="/" render={(renderProps) => {
            return (
              <ImageList {...renderProps} images={this.props.car.images} />
            )
          }} />

          <Route exact path="/:id/images/:imageIndex" render={(renderProps) => {
            return (
              <Image {...renderProps}
                image={this.props.car.images[renderProps.match.params.imageIndex]}
              />
            )
          }} />
        </div>
      )
    // Otherwise, render a loading indicator.
    } else {
      content = (
        <div className="loading">
          <GlobalLoading />
        </div>
      )
    }

    return (
      <Grid container justify="center">
        <Route path="/:id?" render={(renderProps) => {
          return (
            <Header {...renderProps} title={this.props.car.title} />
          )
        }} />

        <Grid item xs={11}>
          { content }
        </Grid>
      </Grid>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    car: state.car
  }
}

App.propTypes = exact({
  dispatch: PropTypes.func.isRequired,
  car: PropTypes.object,
  ...routeProps
})

export { App }
export default connect(mapStateToProps)(App)
