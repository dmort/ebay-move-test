// Main entry point for the front-end client.
// ------------------------------------------

import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'

import Root from './components/root'

// The root DOM element where we will render our React root component
const rootEl = document.getElementById('content')

// Render the root component into the DOM.
const render = (RootComponent) =>
  ReactDOM.render(
    // Enable HMR by wrapping our root component with a react-hot-loader
    // container.
    <AppContainer>
      <RootComponent />
    </AppContainer>,
    rootEl
  )

// Accept all HMR patches that reach the root component and re-render.
if (module.hot)
  module.hot.accept('./components/root', () => render(Root))

// Start our app.
render(Root)
