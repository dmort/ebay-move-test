// A theme for material-ui
// -----------------------

import grey from '@material-ui/core/colors/grey';

const theme = {
  typography: {
    fontFamily: "'Source Sans Pro', sans-serif"
  },
  palette: {
    primary: grey
  },
  mixins: {
    toolbar: {
      minHeight: 50
    }
  }
}

export default theme
