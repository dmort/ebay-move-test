// Create and return a Redux store.
// --------------------------------

import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import reducer from '../client/reducers'

const create = (preloadedState, composeEnhanced = compose) => {
  return createStore(reducer,
    preloadedState,
    composeEnhanced(
      // Apply redux thunk middleware to allow async actions.
      applyMiddleware(thunk),
    )
  )
}

export default create
