export const parseImageUrls = (images, id) => {
  const imageProtocol = 'https://'
  const imageSuffixSmall = '_2.jpg'
  const imageSuffixLarge = '_27.jpg'

  return images.map((image, index) => ({
    ...image,
    urlSmall: `${imageProtocol}${image.uri}${imageSuffixSmall}`,
    urlLarge: `${imageProtocol}${image.uri}${imageSuffixLarge}`,
    viewImageUrl: `/${id}/images/${index}`
  }))
}
