// Web server for serving the index HTML and other static assets.  Calls to the
// API are proxied through to the API server.
// ----------------------------------------------------------------------------

const Koa = require('koa')
const Router = require('koa-router')
const logger = require('koa-logger')
const proxy = require('koa-proxy')
const serve = require('koa-static')
const config = require('config')

const server = new Koa()
const carRoutes = new Router()

import { viewCar } from './controllers/cars'

// Car Routes
carRoutes.get('/', viewCar)
carRoutes.get('/:id/images/:imageIndex', viewCar)
server.use(carRoutes.routes())

// Log requests.
server.use(logger())

// Proxy the request for client.js to the webpack dev server in development.
if (process.env.NODE_ENV === 'development') {
  server.use(proxy({
    host: `http://localhost:${config.WebpackDevServerPort}`,
    match: /\/client.js/
  }))
}

// Serve the static assets.
server.use(serve('./static'))

// Start the web server.
server.listen(config.WebServerPort)

console.log(`web server listening on ${config.WebServerPort}...`)
