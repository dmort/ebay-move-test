// Cars controller
// ---------------

import React from 'react'
import axios from 'axios'
import { renderToString } from 'react-dom/server'
import Grid from '@material-ui/core/Grid'
import { Provider } from 'react-redux'
import { StaticRouter } from 'react-router-dom'
import { SheetsRegistry } from 'react-jss/lib/jss'
import JssProvider from 'react-jss/lib/JssProvider'
import {
  MuiThemeProvider,
  createMuiTheme,
  createGenerateClassName
} from '@material-ui/core/styles'

import App from '../../client/components/App'
import createStore from '../../store/create'
import muiTheme from '../../client/styles/mui-theme'
import { parseImageUrls } from '../parsers'
import html from '../templates/html'

const fetchCarUrl = 'https://www.mobile.de/hiring-challenge.json'

const theme = createMuiTheme(muiTheme)

// View a car.
export const viewCar = async ctx => {
  await axios.get(fetchCarUrl)
    .then(response => {
      const car = {
        ...response.data,
        images: parseImageUrls(response.data.images, response.data.id)
      }
      const store = createStore({car})
      const sheetsRegistry = new SheetsRegistry()
      const generateClassName = createGenerateClassName()

      // Render the React app.
      const reactRoot = renderToString((
        <Provider store={store}>
          <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
            <StaticRouter context={{}} location={ctx.request.url} >
              <MuiThemeProvider theme={theme} sheetsManager={new Map()}>
                <App />
              </MuiThemeProvider>
            </StaticRouter>
          </JssProvider>
        </Provider>
      ))

      const jss = sheetsRegistry.toString()

      ctx.body = html(reactRoot, jss, store)
      ctx.status = 200
    })
    .catch(error => {
      console.error(error)
    })
}
