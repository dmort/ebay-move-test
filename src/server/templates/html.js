// Base HTML template
// ------------------

const html = (reactRoot, jss, store) => {
  return `
    <!doctype html>

    <html>
      <head>
        <meta charset=utf-8>
        <!-- Mobile devices -->
        <meta name=viewport content="width=device-width, initial-scale=1">
        <!-- Icons for material ui -->
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons"
          rel="stylesheet">
        <!-- A global font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro"
          rel="stylesheet">
      </head>
      <body>
        <!-- React root element -->
        <div id="content"><div>${reactRoot}</div></div>
        <!-- SSR JSS as CSS -->
        <style id="jss-server-side">${jss}</style>
        <!-- Preloaded Redux State -->
        <script>
          window.__PRELOADED_STATE__ =
            ${JSON.stringify(store.getState()).replace(/</g, '\\u003c')}
        </script>
        <!-- Client -->
        <script src="/client.js" type="text/javascript"></script>
      </body>
    </html>
  `
}

export default html
