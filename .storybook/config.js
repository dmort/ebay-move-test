import { configure } from '@storybook/react'

function loadStories() {
  require('../src/client/components/stories.js')
}

configure(loadStories, module)
