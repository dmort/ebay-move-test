# Instructions to run the app
- install packages with `npm install`
- run the local server with `npm run dev`
- navigate to localhost:3000 in a browser to load the app
- run the unit tests with `npm run test:unit:all`
