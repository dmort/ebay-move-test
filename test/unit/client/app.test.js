import React from 'react'
import { mount } from 'enzyme'
import { Provider } from 'react-redux'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { BrowserRouter as Router } from 'react-router-dom'

import createStore from '../../../src/store/create'
import { App } from '../../../src/client/components/App'
import muiTheme from '../../../src/client/styles/mui-theme'

const store = createStore()
const theme = createMuiTheme(muiTheme)
let dispatch = jest.fn()

describe('The client', function() {
  const props = {
    dispatch
  }

  it('should render without throwing an error', () => {
    expect(mount(
      <Provider store={store}>
        <Router>
          <MuiThemeProvider theme={theme}>
            <App {...props} />
          </MuiThemeProvider>
        </Router>
      </Provider>
    ).find('.loading').length).toBe(1)
  })
})
