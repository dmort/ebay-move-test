import reducer from '../../../src/client/reducers/index'
import { createInitialState } from './helpers'

describe('The root reducer', () => {
  let initialState = null

  beforeAll(() => {
    initialState = createInitialState()
  })

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual({})
  })
})
