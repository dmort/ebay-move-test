import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'

import reducer from '../../../src/client/reducers/index'
import root from '../../../src/client/reducers/root'

export const createInitialState = () => {
  return {}
}
