import { parseImageUrls } from '../../../src/server/parsers'
import car from '../fixtures/responses/car'

let parsedImages
const carId = 97045

describe('parseImageUrls()', () => {
  beforeAll(() => {
    parsedImages = parseImageUrls(car.images, carId)
  })

  it('should add URLs to be used as image src attributes', () => {
    expect(parsedImages[0].urlSmall)
      .toEqual('https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$_2.jpg')

    expect(parsedImages[0].urlLarge)
      .toEqual('https://i.ebayimg.com/00/s/NjgyWDEwMjQ=/z/znIAAOxynwlTcg-F/$_27.jpg')
  })

  it("should add a URL for the anchor tag's href attribute", () => {
    expect(parsedImages[0].viewImageUrl)
      .toEqual('/97045/images/0')
  })
})
