import { host } from "storybook-host"

// Return a decorator config for the `host` addon.
export const frame = (title, width = 600) => {
  return host({
    title,
    width,
    align: 'center'
  })
}
